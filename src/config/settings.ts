export const settings = {
	server: {
		port: 8080,
		host: '0.0.0.0',
		name: 'api-typescript'
	}
}