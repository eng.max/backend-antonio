import { usersRoute } from "./users/users.route";
import { settings } from "./config/settings";
import * as restify from "restify";

export class ApiServer {
 	static server: ApiServer;
	public api: restify.Server & { users?: Array<any>};

  constructor() {
    this.api = restify.createServer({
      name: settings.server.name
    });

	ApiServer.server = this;

    this.api.use(restify.plugins.bodyParser());

    this.api.get("/", (req, res, next) => {
      res.send(200, { teste: "Hello world!" });
      next();
    });

    new usersRoute(this.api).apply();
  }

  start() {
	this.api.listen(settings.server.port, settings.server.host,() => {
		console.log('Api is running on port 8080.')
	})
  }
}

export const getServer = () => ApiServer.server;
export const getApi = () => ApiServer.server.api;