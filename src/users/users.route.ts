import { ApiServer, getApi } from '../api';
import * as restify from 'restify';
import { Server } from "restify";


const verifyUser = (req, res, next) => {
	if(!req.body) {
		return res.send(400, { message: 'body is required.' })
	}

	if(!req.body.name) {
		return res.send(400, { message: 'name is required.' })
	}

	return next();
};

const verifyUser2 = (api) => (req, res, next) => {
	if(!req.body) {
		return res.send(400, { message: 'body is required.' })
	}

	if(!req.body.name) {
		return res.send(400, { message: 'name is required.' })
	}

	if(!req.body.city) {
		return res.send(400, { message: 'city is required.' })
	}

	return next();
};

const saveUser = (req, res, next) => {
  if (!getApi().users) {
    getApi().users = [];
  }

  const user = req.body;
  getApi().users.push(user);
  res.send(201, user);
};

export class usersRoute {
	
	constructor(public api: Server  & { users?: Array<any> }) {
		
	}

	apply() {
		this.api.get("/users", restify.plugins.conditionalHandler([
			{
				version: ['1.0.0', '2.0.0'], handler: [
					(req, res, next) => {
						console.log(this.api.users);
						
					  res.send(200, this.api.users || []);
					}
				]
			}
		]))

		this.api.get("/users/:id", restify.plugins.conditionalHandler([
			{
				version: ['1.0.0', '2.0.0'], handler: [
					(req, res, next) => {
						const { id } = req.params
						console.log(id);
						
					 	res.send(200, this.api.users.splice(id));
					}
				]
			}
		]))
	  
		this.api.post("/users", restify.plugins.conditionalHandler([
			{version: '1.0.0', handler: [verifyUser, saveUser]},
			{version: '2.0.0', handler: [verifyUser2, saveUser]}
		]));

		// this.api.del('/usres/:id')
	}

}
 