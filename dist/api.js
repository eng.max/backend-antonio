"use strict";
exports.__esModule = true;
var users_route_1 = require("./users/users.route");
var settings_1 = require("./config/settings");
var restify = require("restify");
var ApiServer = /** @class */ (function () {
    function ApiServer() {
        this.api = restify.createServer({
            name: settings_1.settings.server.name
        });
        ApiServer.server = this;
        this.api.use(restify.plugins.bodyParser());
        this.api.get("/", function (req, res, next) {
            res.send(200, { teste: "Hello world!" });
            next();
        });
        new users_route_1.usersRoute(this.api).apply();
    }
    ApiServer.prototype.start = function () {
        this.api.listen(settings_1.settings.server.port, settings_1.settings.server.host, function () {
            console.log('Api is running on port 8080.');
        });
    };
    return ApiServer;
}());
exports.ApiServer = ApiServer;
exports.getServer = function () { return ApiServer.server; };
exports.getApi = function () { return ApiServer.server.api; };
