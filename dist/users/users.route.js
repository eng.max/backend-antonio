"use strict";
exports.__esModule = true;
var api_1 = require("../api");
var restify = require("restify");
var verifyUser = function (req, res, next) {
    if (!req.body) {
        return res.send(400, { message: 'body is required.' });
    }
    if (!req.body.name) {
        return res.send(400, { message: 'name is required.' });
    }
    return next();
};
var verifyUser2 = function (api) { return function (req, res, next) {
    if (!req.body) {
        return res.send(400, { message: 'body is required.' });
    }
    if (!req.body.name) {
        return res.send(400, { message: 'name is required.' });
    }
    if (!req.body.city) {
        return res.send(400, { message: 'city is required.' });
    }
    return next();
}; };
var saveUser = function (req, res, next) {
    if (!api_1.getApi().users) {
        api_1.getApi().users = [];
    }
    var user = req.body;
    api_1.getApi().users.push(user);
    res.send(201, user);
};
var usersRoute = /** @class */ (function () {
    function usersRoute(api) {
        this.api = api;
    }
    usersRoute.prototype.apply = function () {
        var _this = this;
        this.api.get("/users", restify.plugins.conditionalHandler([
            {
                version: ['1.0.0', '2.0.0'], handler: [
                    function (req, res, next) {
                        res.send(200, _this.api.users || []);
                    }
                ]
            }
        ]));
        this.api.get("/users/:id", restify.plugins.conditionalHandler([
            {
                version: ['1.0.0', '2.0.0'], handler: [
                    function (req, res, next) {
                        var id = req.params.id;
                        res.send(200, _this.api.users.indexOf(id));
                    }
                ]
            }
        ]));
        this.api.post("/users", restify.plugins.conditionalHandler([
            { version: '1.0.0', handler: [verifyUser, saveUser] },
            { version: '2.0.0', handler: [verifyUser2, saveUser] }
        ]));
        // this.api.del('/usres/:id')
    };
    return usersRoute;
}());
exports.usersRoute = usersRoute;
